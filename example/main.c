#include "ecbm_core.h"	//加载库函数的头文件。
#include "modbus.h"

void uart1_receive_callback(void){
    ecbm_modbus_rtu_receive();
}
void ecbm_modbus_rtu_set_data(u8 dat){
    uart_char(1,dat);
}
u8   ecbm_modbus_rtu_get_data(void){
    return SBUF;
}
void tim0_fun()TIMER0_IT_NUM{
    ECBM_MODBUS_RTU_TIMEOUT_RUN();
}

void main(){			//main函数，必须的。
	system_init();		//系统初始化函数，也是必须的。
	timer_init();
	timer_start(0);
	while(1){
		ecbm_modbus_rtu_run();
	}
}







/*-------------------------------------------------------
读输入离散量函数。
-------------------------------------------------------*/
#if ECBM_MODBUS_RTU_CMD02_EN
emu8 ecbm_modbus_cmd_read_io_bit(emu16 addr,emu8 * dat){
    switch(addr){
    case  0:*dat=P00;break;
    case  1:*dat=P01;break;
    case  2:*dat=P02;break;
    case  3:*dat=P03;break;
    case  4:*dat=P04;break;
    case  5:*dat=P05;break;
    case  6:*dat=P06;break;
    case  7:*dat=P07;break;
    case  8:*dat=P10;break;
    case  9:*dat=P11;break;
    case 10:*dat=P12;break;
    case 11:*dat=P13;break;
    case 12:*dat=P14;break;
    case 13:*dat=P15;break;
    case 14:*dat=P16;break;
    case 15:*dat=P17;break;
    case 16:*dat=P20;break;
    case 17:*dat=P21;break;
    case 18:*dat=P22;break;
    case 19:*dat=P23;break;
    case 20:*dat=P24;break;
    case 21:*dat=P25;break;
    case 22:*dat=P26;break;
    case 23:*dat=P27;break;
    case 24:*dat=P30;break;
    case 25:*dat=P31;break;
    case 26:*dat=P32;break;
    case 27:*dat=P33;break;
    case 28:*dat=P34;break;
    case 29:*dat=P35;break;
    case 30:*dat=P36;break;
    case 31:*dat=P37;break;
    default:*dat=1;return ECBM_MODBUS_RTU_IO_ADDR_ERR;break;
    }
    return ECBM_MODBUS_RTU_OK;
}
#endif
/*-------------------------------------------------------
读输入寄存器函数。
-------------------------------------------------------*/
#if ECBM_MODBUS_RTU_CMD04_EN
emu8 ecbm_modbus_cmd_read_io_reg(emu16 addr,emu16 * dat){
    switch (addr){
    case 0:*dat=(emu16)P0;break;
    case 1:*dat=(emu16)P1;break;
    case 2:*dat=(emu16)P2;break;
    case 3:*dat=(emu16)P3;break;
    case 4:*dat=(emu16)P4;break;
    case 5:*dat=(emu16)P5;break;
    case 6:*dat=(emu16)P6;break;
    case 7:*dat=(emu16)P7;break;
    default:*dat=0xffff;return ECBM_MODBUS_RTU_IO_ADDR_ERR;break;
    }
    return ECBM_MODBUS_RTU_OK;
}
#endif